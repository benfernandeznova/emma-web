import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthComponent } from './auth.component';
//importing routes
import { routes } from './auth-routing.module';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

describe('AuthRoutingModule', () => {
  let router: Router;
  let fixture: ComponentFixture<AuthComponent>;
  let location: Location;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes)],
      declarations: [AuthComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    fixture = TestBed.createComponent(AuthComponent);
    router.initialNavigation();
  });

  it('should test redirection to default path', () => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(location.path()).toBe('/login');
    });
  });
});
