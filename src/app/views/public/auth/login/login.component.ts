import { DOCUMENT } from '@angular/common';
import { Component, Inject, Renderer2 } from '@angular/core';
//import { MatSelectChange } from '@angular/material/select';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  readonly themeAnchor = this.document.getElementById('app-theme');

  constructor(
    private router: Router,
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2
  ) {}

  login() {
    this.router.navigate(['/dashboard']);
  }

  // setTheme({ source }: MatSelectChange) {
  //   if (source.value === 'light') {
  //     this.renderer.setAttribute(this.themeAnchor, 'href', '/light-theme.css');
  //   } else {
  //     this.renderer.setAttribute(this.themeAnchor, 'href', '/light-theme2.css');
  //   }
  // }
}
