import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { DashboardComponent } from './dashboard.component';
//importing routes
import { routes } from './dashboard-routing.module';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

describe('AuthRoutingModule', () => {
  let router: Router;
  let fixture: ComponentFixture<DashboardComponent>;
  let location: Location;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes)],
      declarations: [DashboardComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    fixture = TestBed.createComponent(DashboardComponent);
    router.initialNavigation();
  });

  it('should test redirection to default path', () => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(location.path()).toBe('/');
    });
  });
});
